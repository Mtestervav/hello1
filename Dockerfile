FROM python:3.8.10-alpine

ENV PYTHONUNBUFFERED 1

RUN pip install --upgrade pip

RUN apk add gcc


COPY ./requirements.txt .
RUN pip install -r requirements.txt

COPY ./pushka /app

WORKDIR /app


COPY ./entrypoint.sh /
ENTRYPOINT ["sh", "/entrypoint.sh"]

RUN echo "ПРОШЛО ЭХО"

RUN python3 manage.py migrate --no-input
RUN python3 manage.py collectstatic --no-input

