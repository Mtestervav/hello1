#!/usr/bin sh

python3 manage.py migrate --no-input
python3 manage.py collectstatic --no-input
python3 manage.py createsuperuser --username "admin" --email "mvav87@mail.ru" --password "superuser"
gunicorn pushka.wsgi:application --bind 0.0.0.0:8000

